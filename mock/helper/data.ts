/*
 * @Author: xuyijie
 * @Date: 2022-12-06 08:39:46
 * @LastEditors: xuyijie
 * @LastEditTime: 2022-12-06 08:51:21
 * @Description: 文件说明
 */
export const user = {
  id: 1,
  name: '老乡',
  email: '2696211898@qq.com',
  sex: 1,
  avatar: '/images/logoVue.png',
  home: '',
  weibo: null,
  wechat: '',
  github: '',
  qq: '',
  created_at: '2019-03-13T20:50:56.000000Z',
  updated_at: '2022-10-23T15:19:54.000000Z',
} as any
